# Adding New Icons

!!IMPORTANT

The icons package has a custom build process, as it generates its both stripped
svgs and glyphs that are committed to the repo, so that they can be accessed as
paths when published.

When adding a new icon, or making changes to an existing one, run `yarn update`
from within the `packages/elements/icon` directory.

## Generating an svg

## Building stripped svg and glyphs

## Verifying and committing your new icon
