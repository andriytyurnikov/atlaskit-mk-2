# Empty state

Empty State is used for various scenarios, for example: empty search, no items,
broken link, general error message, welcome screen etc. (usually it takes the
whole page).

![Example empty state](https://imgur.com/llMvEF1)
