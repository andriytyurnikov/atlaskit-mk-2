# @atlaskit/analytics-next

## 1.0.2
- [patch] Adds action key to analytics payload type [7deeaef](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7deeaef)

## 1.0.1
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 1.0.0
- [major] release @atlaskit/analytics-next package [80695ea](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/80695ea)
