// @flow

import styled from 'styled-components';

export const Heading = styled.div`
  display: flex;
  padding: 4px 0 8px 0;
  font-weight: bold;
`;

export const MonthAndYear = styled.div`
  flex-basis: 100%;
  text-align: center;
`;
