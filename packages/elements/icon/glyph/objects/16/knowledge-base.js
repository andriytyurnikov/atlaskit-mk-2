'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects16KnowledgeBaseIcon = function Objects16KnowledgeBaseIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M6 4h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2zm5 12a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm-3-5.848c0 1.536 1.526 3.105 1.526 3.105.26.268.473.77.473 1.115 0 .346.3.628.667.628h2.668c.367 0 .667-.282.667-.628 0-.345.213-.847.473-1.115 0 0 1.526-1.569 1.526-3.105C16 7.86 14.21 6 12 6s-4 1.859-4 4.152zM12.3 13c.183-.428.439-.827.74-1.138a4.81 4.81 0 0 0 .523-.675c.273-.42.428-.799.437-1.02C14 8.947 13.088 8 12 8s-2 .947-2 2.152c.01.236.164.616.437 1.035.207.316.414.564.524.677.301.31.556.708.739 1.136h.6z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects16KnowledgeBaseIcon;