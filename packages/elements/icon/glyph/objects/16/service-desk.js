'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects16ServiceDeskIcon = function Objects16ServiceDeskIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M13 9.1V8a1 1 0 0 0-2 0v1.1A5.002 5.002 0 0 0 7 14h10a5.002 5.002 0 0 0-4-4.9zM6 4h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2zm1 11a1 1 0 0 0 0 2h10a1 1 0 0 0 0-2H7z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects16ServiceDeskIcon;