// @flow
import React from 'react';
import { md, Example } from '@atlaskit/docs';

export default md`
  Use the logo component to output SVG versions of the company and product logos.

  ## Examples

  ${(
    <Example
      Component={require('../examples/0-basic').default}
      title="Basic"
      source={require('!!raw-loader!../examples/0-basic')}
    />
  )}

`;
