# Inline Edit

A component for displaying and editing values in fields.

## Installation

npm install @atlaskit/inline-edit

## Usage

See [here](https://ak-mk-2-prod.netlify.com/mk-2/packages/elements/inline-edit)
