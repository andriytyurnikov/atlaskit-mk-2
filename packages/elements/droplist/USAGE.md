# Droplist

This is a base (internal) component which should be used to build all the 'dropdown' lists.

## Installation

npm install @atlaskit/droplist

## Usage

See [here](https://ak-mk-2-prod.netlify.com/mk-2/packages/elements/droplist)
