// @flow

export type Event = { target: { value: string } };
export type Handler = (e: any) => void;
