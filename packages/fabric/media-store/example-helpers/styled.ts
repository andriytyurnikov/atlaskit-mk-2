import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
`;

export const ImagePreview = styled.img`
  width: 300px;
`;

export const PreviewWrapper = styled.div`
  flex: 1;
`;

export const MetadataWrapper = styled.pre`
  width: 400px;
  overflow: scroll;
  flex: 1;
`;
