import styled from 'styled-components';

export const TriggerWrapper = styled.div`
  display: flex;
`;

export const ExpandIconWrapper = styled.span`
  margin-left: -8px;
`;

export const Wrapper = styled.span`
  display: flex;
  align-items: center;
  div {
    display: flex;
  }
`;

export const Spacer = styled.span`
  display: flex;
  flex: 1;
  padding: 12px;
`;
