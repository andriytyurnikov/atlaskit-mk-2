import { akColorN30 } from '@atlaskit/util-shared-styles';
import styled from 'styled-components';

// tslint:disable-next-line:variable-name
const Separator = styled.span`
  background: ${akColorN30};
  height: 100%;
  padding-left: 1px;
  margin: 2px 8px;
`;

export default Separator;
