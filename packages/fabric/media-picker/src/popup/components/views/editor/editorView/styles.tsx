import styled from 'styled-components';

export const EditorContainer = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`;
