import * as React from 'react';

// tslint:disable-next-line:variable-name
const TableCell = props => <td>{props.children}</td>;
export default TableCell;
