import * as React from 'react';

// tslint:disable-next-line:variable-name
const TableHeader = props => <th>{props.children}</th>;
export default TableHeader;
