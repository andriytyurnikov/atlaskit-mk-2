# Atlaskit MK II

This is a spike of the potential new build system to be shared by Atlaskit, Media and Fabric.

See [CONTRIBUTING.md](CONTRIBUTING.md) for more details on how to get started.
